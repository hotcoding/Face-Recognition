package com.hello.facerecognition.com.hello.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import com.hello.facerecognition.Application.BaseApplication;

import java.util.UUID;
import java.util.logging.Logger;

public class CameraUtils {

    /**
     * 检查是否有相机的权限
     * @param context
     * @return
     */
    private static boolean checkCameraHardware(Context context){
        System.out.println(context == null);
        if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        }
        return false;
    }

    /**
     * 获取摄像头实例
     * @return
     */
    public static Camera getCameraInstance(int position){
        Camera camera = null;
        if(checkCameraHardware(BaseApplication.context)) {
            try {
                camera = Camera.open(position);
            } catch (Exception e) {
                Logger.getGlobal().info("Error: " + e.toString());
            }
        }
        return camera;
    }

    public static String getUUID() {
        String uuid = UUID.randomUUID().toString();
        //去掉“-”符号
        return uuid.replaceAll("-", "");
    }


}
