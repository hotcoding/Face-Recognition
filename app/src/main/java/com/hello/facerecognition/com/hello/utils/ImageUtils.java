package com.hello.facerecognition.com.hello.utils;

import android.util.Base64;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;


/**
 * 将图片转换为Base64<br>
 * 将base64编码字符串解码成img图片
 * @创建时间 2015-06-01 15:50
 *
 */
public class ImageUtils {

    /**
     * 将图片转换成Base64编码
     * @param imgFile 待处理图片
     * @return
     */
    /**
     * 将图片转换成Base64编码的字符串
     * @param path
     * @return base64编码的字符串
     */
    public static String imageToBase64(String path){
        if(StringUtils.isNullOrEmpty(path)){
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try{
            is = new FileInputStream(path);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = Base64.encodeToString(data,Base64.DEFAULT);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(null !=is){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return "data:image/"+getFileExt(path)+";base64,"+result;
    }

    public static String getFileExt(String path){
        if(StringUtils.isNullOrEmpty(path)){
            return null;
        }
        String[] arr = path.split("\\.");
        if(arr.length<1){
            return "";
        }
        return arr[arr.length-1];
    }
}