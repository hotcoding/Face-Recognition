package com.hello.facerecognition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int CAMERA_REQUEST = 1888;
    private ImageButton inputButton,recognitionButton,loginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputButton = findViewById(R.id.imageButton);
        recognitionButton = findViewById(R.id.imageButton3);
        loginButton = findViewById(R.id.imageButton2);
        loginButton.setOnClickListener(this);
        inputButton.setOnClickListener(this);
        recognitionButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageButton:{
                Intent intent = new Intent(this,RegisterActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.imageButton3:{
                startActivity(new Intent(this,ShowActivity.class));
                break;
            }
            case R.id.imageButton2:{
                Intent intent = new Intent(this,RegisterActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
