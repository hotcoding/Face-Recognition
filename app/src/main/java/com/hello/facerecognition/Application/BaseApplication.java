package com.hello.facerecognition.Application;

import android.app.Application;
import android.content.Context;
import android.hardware.Camera;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.zhy.http.okhttp.log.LoggerInterceptor;
import okhttp3.OkHttpClient;

import java.util.concurrent.TimeUnit;

public class BaseApplication extends Application {
    public static Context context;
    private static BaseApplication baseApplication = null;
    public static OkHttpClient okHttpClient = null;
    public static Integer CameraFront;
    public static Integer CameraBack;

    @Override
    public void onCreate(){
        super.onCreate();
        baseApplication = this;
        context = this.getApplicationContext();
        getCameraInfo();
        okHttpClient = new OkHttpClient.Builder().addInterceptor(new LoggerInterceptor("TAG"))
                .addNetworkInterceptor( new StethoInterceptor())//增加Stetho拦截器,用于调试
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS).build();
    }

    public static void getCameraInfo(){
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Integer iCameraCnt = Camera.getNumberOfCameras();

        for (int i = 0; i < iCameraCnt; i++) {
            Camera.getCameraInfo(i,cameraInfo);
            if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){
                CameraFront = i;
            }
            else if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK){
                CameraBack = i;

            }

        }
    }

    public static BaseApplication getInstance(){
        return baseApplication;
    }
}
